class Dog:
    def __init__(self):
        self.name = 'Milki'
        self.age = 1

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age


dog1 = Dog()
dog2 = Dog()

dog2.birthday()

print(dog1.get_age())
print(dog2.get_age())
