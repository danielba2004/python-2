class BigThing:

    def __init__(self, number):
        self._number = number

    def size(self):
        return self._number


class BigCat(BigThing):
    def __init__(self, number, weight):
        BigThing.__init__(self, number)
        self._weight = weight

    def size(self):
        super().size()
        if self._weight > 20:
            return "very fat"
        elif self._weight > 15:
            return "fat"
        else:
            return "ok"


cutie = BigCat(10, 22)
print(cutie.size())
