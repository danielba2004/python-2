class Pixel:
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_grayscale(self):
        gray = (self._red + self._green + self._blue)/3
        self._red = gray
        self._green = gray
        self._blue = gray

    def print_pixel_info(self):
        str1 = "X: %s, Y: %s, Color = (%d, %d, %d)" % (self._x, self._y, self._red, self._green, self._blue)
        list1 =  [x for x in [self._red, self._green, self._blue] if x != 0]
        if len(list1) == 1:
            if self._red != 0:
                str1 += " Red"
            elif self._green != 0:
                str1 += " Green"
            else:
                str1 += " Blue"
        print(str1)


def main():
    p1 = Pixel(5, 6, 250, 0, 0)
    p1.print_pixel_info()
    p1.set_grayscale()
    p1.print_pixel_info()


main()



