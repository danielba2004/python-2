class Dog:
    count_animals = 0
    def __init__(self, name='milki'):
        self._name = name
        self._age = 1
        Dog.count_animals += 1

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name


def main():
    dog1 = Dog('Mishmish')
    dog2 = Dog()

    dog2.birthday()

    print(dog1.get_name())
    print(dog2.get_name())
    dog2.set_name("Momo")
    print(dog2.get_name())
    print(Dog.count_animals)

main()