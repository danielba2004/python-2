class UnderAge(Exception):
    def __init__(self, age):
        self._age = age

    def __str__(self):
        return "your age is below 18\nyour age is %d\nyou can come back in %d years :)" % (self._age, 18-self._age)





def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(age)
    except UnderAge as e:
        print(e.__str__())
        print("sorry")


send_invitation("daniel", 17)
