import string


class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, username, character):
        self._username = username
        self._character = character

    def __str__(self):
        return "The username '%s' contains an illegal character\nthe illegal character is '%s' (index %d)" % \
        (self._username, self._character, self._username.index(self._character))


class UsernameTooShort(Exception):

    def __str__(self):
        return "username is too short"


class UsernameTooLong(Exception):

    def __str__(self):
        return "username is too long"


class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "The password is missing a character"


class Uppercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Uppercase)"


class Lowercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Lowercase)"


class Digit(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Digit)"


class Spacial(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Special)"


class PasswordTooLong(Exception):
    def __str__(self):
        return "password is too long"


class PasswordTooShort(Exception):
    def __str__(self):
        return "password is too short"


def check_input(username, password):
    try:
        if 16 < len(username):
            raise UsernameTooLong
        elif 3 > len(username):
            raise UsernameTooShort
        else:
            for letter in username:
                if letter in string.punctuation and letter != '_':
                    raise UsernameContainsIllegalCharacter(username, letter)
        if len(password) > 40:
            raise PasswordTooLong
        elif len(password) < 8:
            raise PasswordTooShort
        else:
            capital, small, number, spacial = 0, 0, 0, 0
            for letter in password:
                if letter in string.punctuation:
                    spacial += 1
                elif letter.isupper():
                    capital += 1
                elif letter.islower():
                    small += 1
                else:
                    number += 1
            if capital == 0:
                raise Uppercase
            elif small == 0:
                raise Lowercase
            elif number == 0:
                raise Digit
            elif spacial == 0:
                raise Spacial
    except Exception as e:
        print(e.__str__())
        return False
    else:
        print("OK")
        return True


# check_input("1", "2")
# check_input("0123456789ABCDEFG", "2")
# check_input("A_a1.", "12345678")
# check_input("A_1", "2")
# check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
# check_input("A_1", "abcdefghijklmnop")
# check_input("A_1", "ABCDEFGHIJLKMNOP")
# check_input("A_1", "ABCDEFGhijklmnop")
# check_input("A_1", "4BCD3F6h1jk1mn0p")
# print("1")
# check_input("A_1", "4BCD3F6.1jk1mn0p")


def main():
    while not check_input(input("enter username: "), input("enter password: ")):
        pass


main()