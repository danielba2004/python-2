def read_file(file_name):
    print("__CONTENT_START__")
    try:
        with open(file_name, 'r') as f:
            print(f.read())
    except:
        print("__NO_SUCH_FILE__")
    finally:
        print("__CONTENT_END__")


read_file("file_does_exist.txt")
