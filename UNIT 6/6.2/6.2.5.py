from file2 import BirthdayCard
from file1 import GreetingCard


def main():
    b_card = BirthdayCard()
    g_card = GreetingCard()
    b_card.greeting_msg()
    print("*\n*\n*\n*")
    g_card.greeting_msg()


if __name__ == "__main__":
    main()
