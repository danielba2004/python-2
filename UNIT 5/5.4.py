import functools


def check_id_valid(id_number):
    num = ""
    index = 1
    for digit in str(id_number):
        if index % 2 == 0:
            if int(digit) * 2 > 9:
                num += str(int((int(digit) * 2) / 10) + (int(digit) * 2) % 10)
            else:
                num += str(int(digit) * 2)
        else:
            num += digit
        index += 1
    return functools.reduce(func1, num) % 10 == 0


def func1(a, b):
    return int(a) + int(b)

# print(check_id_valid(214422669))
# print(check_id_valid(123456782))


class IDIterator:
    def __init__(self, id1):
        self._id1 = id1

    def __iter__(self):
        return self

    def __next__(self):
        if self._id1 == 999999999:
            raise StopIteration

        self._id1 += 1
        while not check_id_valid(self._id1):
            self._id1 += 1
        return self._id1


def id_generator(id2):
    return (id for id in range(id2, 999999999) if check_id_valid(id))


def main():
    id_num = input("enter id number: ")

    it_or_gen = input("Generator or Iterator? (gen/it)? ")
    while it_or_gen != "it" and it_or_gen != "gen":
        it_or_gen = input("Generator or Iterator? (gen/it)? ")

    if it_or_gen == "it":
        id_iter = IDIterator(int(id_num))
        for i in range(0, 10):
            print(next(id_iter))
    else:
        id_gen = id_generator(int(id_num))
        for i in range(0, 10):
            print(next(id_gen))


if __name__ == "__main__":
    main()
