class MusicNotes:
    def __init__(self):
        self._la = 55
        self._si = 61.74
        self._do = 65.41
        self._re = 73.42
        self._mi = 82.41
        self._fa = 87.31
        self._sol = 98
        self._index = -1
        self._notes_lost = [self._la, self._si, self._do, self._re, self._mi, self._fa, self._sol]

    def __iter__(self):
        return self

    def __next__(self):
        if self._index == 34:
            raise StopIteration
        self._index += 1
        return self._notes_lost[self._index % 7] * (2 ** (int(self._index / 7)))


notes_iter = iter(MusicNotes())
for freq in notes_iter:
    print(freq)
