numbers = iter(list(range(1, 101)))
next(numbers)
next(numbers)
for i in numbers:
    try:
        print(i)
        next(numbers)
        next(numbers)
    except StopIteration:
        break
