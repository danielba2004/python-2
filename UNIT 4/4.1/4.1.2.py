def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    sentence2 = ""
    generator = (words[word] for word in sentence.split(" "))
    for word in generator:
        sentence2 += word + " "
    return sentence2


print(translate("gato esta en la casa"))
