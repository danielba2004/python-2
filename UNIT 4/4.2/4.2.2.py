def parse_ranges(ranges_string):
    numbers_list = []
    first_generator = ([int(seq[0:seq.index("-")]), int(seq[seq.index("-")+1:])] for seq in ranges_string.split(","))
    return (num for start, end in first_generator for num in range(start, end+1))


print(list(parse_ranges("0-0,4-8,20-21,43-45")))

