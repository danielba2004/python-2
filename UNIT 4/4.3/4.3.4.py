def get_fibo():
    yield 0
    yield 1
    x = 0
    y = 1
    temp = 0
    while(True):
        yield x + y
        temp = y
        y += x
        x = temp


fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
