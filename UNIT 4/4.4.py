def gen_secs():
    return (num for num in range(0, 60))


def gen_minutes():
    return (num for num in range(0, 60))


def gen_hours():
    return (num for num in range(0, 24))


def gen_time():
    hour_gen = gen_hours()
    for hour in hour_gen:
        minute_gen = gen_minutes()
        for minute in minute_gen:
            second_gen = gen_secs()
            for second in second_gen:
                yield "%02d:%02d:%02d" % (hour, minute, second)


# time_gen = gen_time()
# for time in time_gen:
#     print(time)


def gen_years(start=2023):
    for year in range(start, 1000000):
        yield year


def gen_months():
    return (month for month in range(1, 13))


def gen_days(month, leap_year=True):
    days = 0
    if month == 1:
        days = 31
    elif month == 2:
        if leap_year:
            days = 29
        else:
            days = 28
    elif month == 3:
        days = 31
    elif month == 4:
        days = 30
    elif month == 5:
        days = 31
    elif month == 6:
        days = 30
    elif month == 7:
        days = 31
    elif month == 8:
        days = 31
    elif month == 9:
        days = 30
    elif month == 10:
        days = 31
    elif month == 11:
        days = 30
    elif month == 12:
        days = 31
    return (day for day in range(0, days+1))


def gen_date():
    years_gen = gen_years()
    for year in years_gen:
        if year % 400 == 0:
            if year % 100 == 0:
                is_leap = False
            else:
                is_leap = True
        elif year % 4 == 0:
            is_leap = True
        else:
            is_leap = False
        month_gen = gen_months()
        for month in month_gen:
            day_gen = gen_days(month, is_leap)
            for day in day_gen:
                time_gen = gen_time()
                for time2 in time_gen:
                    yield "%d:%02d:%02d " % (year, month, day,) + time2


date = gen_date()
count = 0
while True:
    if count % 1000000 != 0:
        next(date)
    else:
        print(next(date))
    count += 1
