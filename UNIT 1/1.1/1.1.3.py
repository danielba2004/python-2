def four_dividers(number):
    return list(filter(func, range(1, number+1)))


def func(number):
    return number % 4 == 0


print(four_dividers(100))
