import functools


def sum_of_digits(number):
    return functools.reduce(func, str(number))


def func(a, b):
    return int(a) + int(b)


print(sum_of_digits(568))