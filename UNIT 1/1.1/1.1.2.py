def double_letter(my_str):
    return "".join(list(map(twice, my_str)))


def twice(char):
    return char*2


print(double_letter("hi you"))


