with open('names.txt', 'r') as f:
    print(max(f.readlines(), key=lambda x: len(x)))


with open('names.txt', 'r') as f:
    print(sum(list(map(lambda x: len(x[0:-1]), f.readlines())))+1, '\n')

with open('names.txt', 'r') as f:
    lines = [x[0:-1] if x[-1] == '\n' else x for x in f.readlines()]
    print("\n".join([x for x in lines if len(x) == len(min(lines, key=lambda x: len(x)))]) + '\n')

with open('name_length.txt', 'w') as f1:
    with open('names.txt', 'r') as f2:
        f1.write("\n".join([str(len(x)) for x in [x[0:-1] if x[-1] == '\n' else x for x in f2.readlines()]]))

num = int(input("enter number of letters:"))
with open('names.txt', 'r') as f2:
    print("\n".join([x for x in [x[0:-1] if x[-1] == '\n' else x for x in f2.readlines()] if len(x) == num]))
