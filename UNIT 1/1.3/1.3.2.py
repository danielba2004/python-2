def is_prime(number):
    return (lambda list1: len(list1) == 2)([x for x in range(1, number+1) if number % x == 0])


print(is_prime(53))
